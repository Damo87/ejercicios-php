<?php
/**
 * Usando PHP, crear una clase llamada ChangeString ​que tenga un método llamado build
 * el cual tome un parámetro string que debe ser modificado por el siguiente algoritmo .
 * Reemplazar cada letra de la cadena con la letra siguiente en el alfabeto. Por ejemplo
 * reemplazar a​ por b​ ó c​ por d.​ Finalmente devolver la cadena.
 * Indicaciones
 *
 * - Crear la solución en un solo archivo llamado ChangeString.php
 * - El método build devuelve la salida del algoritmo
 * - Considerar el siguiente abecedario : a, b, c, d, e, f, g, h, i, j, k, l, m, n, ñ, o, p, q, r, s, t, u, v, w, x, y, z.
 */

class ChangeString{

    function build($string){
        $abecedario = explode(',' , 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,ñ,o,p,q,r,s,t,u,v,w,x,y,z');
        $r = '';
        for ($i = 0 ; $i< strlen($string);$i++){
            $valor = mb_substr($string,$i,1,'UTF-8');
            if (in_array(strtolower($valor), $abecedario) || ord($valor) === 195) {
                if(ord($valor) === 195){
                    $r .= ($valor==='ñ')?'o':'O';
                }elseif(strtolower($valor)==='n'){
                    $r .= ctype_lower($valor)?'ñ':'Ñ';
                }elseif(strtolower($valor)==='z'){
                    $r .= ctype_lower($valor)?'a':'A';
                }else{
                    $r .=++$valor;
                }
            }else{
                $r .= $valor;
            }
        }
        echo $r;
    }

}

//$string = "123 abcd*3";
//$string = "**Casa 52";
$string = "**Casa 52Z";

$changeString = new ChangeString();
$changeString->build($string);


