<?php

class ClearPar{
    private $abrir = '(';
    private $cerrar = ')';

    public function build($cadena){
        $respuesta = '';
        do{
            $posI = strpos($cadena, $this->abrir);
            if($posI !== false){
                $respuesta.= $this->verificarPareja($cadena,$posI);
            }
            $cadena = substr($cadena,$posI+1);
        }while(strlen($cadena)>0);

        echo $respuesta;
    }

    private function verificarPareja($cadena,$posI){
        $cierre = substr($cadena,$posI+1,1);
        if($cierre === $this->cerrar){
            return substr($cadena,$posI,1).$cierre;
        }else{
            return '';
        }
    }

}

$cleanPar = new ClearPar();
$cleanPar->build($argv[1]);




