<?php

use App\Controller\EmpleadoController;

// Routes
/*
$App->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});*/


$app->group('/empleados/', function () {

    $this->get('lista', function ($request, $response, $args) {
        $empleado = new EmpleadoController();
        $respuesta = $empleado->getLista();
        return $this->renderer->render($response, 'index.phtml', [
            'empleados' => $respuesta
        ]);
    });

    $this->get('busqueda', function ($request, $response , $args) {
        $email = $request->getParam('email');
        $empleado = new EmpleadoController();
        $respuesta = $empleado->getXEmail($email);
        return $this->renderer->render($response, 'busqueda.phtml', [
            'empleado' => $respuesta
        ]);
    });

    $this->get('id', function ($request, $response, $args) {
        $id = $request->getParam('id');
        $empleado = new EmpleadoController();
        $respuesta = $empleado->getXId($id);
        return $this->renderer->render($response, 'detalle.phtml', [
            'empleado' => $respuesta
        ]);
    });

    $this->get('xml', function ($request, $response, $args) {
        $minimo = $request->getParam('minimo');
        $maximo = $request->getParam('maximo');
        $empleado = new EmpleadoController();
        $result = $empleado->getXSalario($minimo, $maximo);
        $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Salaries></Salaries>");
        $node = $xml->addChild('request');
        $empleado->array_to_xml($result, $node);
        $response = $response->withHeader('Content-type', 'text/xml');
        $response->getBody()->write($xml->asXML());
        return $response;
    });


});