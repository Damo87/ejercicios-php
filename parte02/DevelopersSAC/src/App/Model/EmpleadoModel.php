<?php

namespace App\Model;

class EmpleadoModel{

    private $empleado = Array();
    
    public function exchangeArray($objeto){
        $this->empleado['id'] = $objeto['id'];
        $this->empleado['name'] = $objeto['name'];
        $this->empleado['email'] = $objeto['email'];
        $this->empleado['phone'] = $objeto['phone'];
        $this->empleado['address'] = $objeto['address'];
        $this->empleado['position'] = $objeto['position'];
        $this->empleado['salary'] = $objeto['salary'];
        $this->empleado['skills'] = $objeto['skills'];
        return $this->empleado;
    }
}
