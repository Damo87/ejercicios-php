<?php

namespace App\Controller;

use App\Model\EmpleadoModel;

class EmpleadoController{

    private $empleados;

    function __construct(){
        $this->empleados = file_get_contents( __DIR__."/../recurso/employees.json");
    }

    public function getLista(){
        $resultados = json_decode($this->empleados, true);
        $lista = Array();
        for ($i=0; $i < count($resultados); $i++) {
            $empleado = new EmpleadoModel();
            $empleado = $empleado->exchangeArray($resultados[$i]);
            array_push($lista, $empleado);
        }
        return $lista;
    }

    public function getXEmail($email){
        $resultados = json_decode($this->empleados, true);
        $index = array_search($email, array_column($resultados, 'email'));
        if (is_bool($index)) {
            return null;
        } else {
            $empleado = new EmpleadoModel();
            $empleado = $empleado->exchangeArray($resultados[$index]);
            return $empleado;
        }
    }

    public function getXId($id){
        $resultados = json_decode($this->empleados, true);
        $index = array_search($id, array_column($resultados, 'id'));
        if(is_bool($index)){
            return null;
        } else {
            $empleado = new EmpleadoModel();
            $empleado = $empleado->exchangeArray($resultados[$index]);
            return $empleado;
        }
    }

    public function getXSalario($minimo, $maximo){
        $resultados = json_decode($this->empleados, true);
        $respuesta = Array();
        $minimo = str_replace(',', '', $minimo);
        $maximo = str_replace(',', '', $maximo);
        for ($i=0; $i < count($resultados); $i++) {
            $salario = substr($resultados[$i]['salary'], 1);
            $salario = str_replace(',', '', $salario);
            if ((int)$salario >= (int)$minimo && (int)$salario <= (int)$maximo) {
                array_push($respuesta, array( 'item' => $resultados[$i]));
            }
        }
        return $respuesta;
    }

    public function array_to_xml($array, &$xml_user_info) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml_user_info->addChild("$key");
                    $this->array_to_xml($value, $subnode);
                }else{
                    $subnode = $xml_user_info->addChild("item$key");
                    $this->array_to_xml($value, $subnode);
                }
            }else {
                $xml_user_info->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

}
