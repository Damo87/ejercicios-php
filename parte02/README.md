# Developers SAC.

## Virtual Host

<VirtualHost *:80>

	ServerName php-developer.com
	DocumentRoot /var/www/php-developer.com/parte02/DevelopersSAC/public/

	<Directory /var/www/php-developer.com/parte02/DevelopersSAC/public/>
		Options Indexes FollowSymLinks Includes ExecCGI
             	AllowOverride All
              	Require all granted
	</Directory>

	LogLevel debug
    ErrorLog  /var/log/httpd/php-developer.com_error.log
    CustomLog /var/log/httpd/php-developer.com_access.log combined

</VirtualHost>

## Lista de Empleados

http://php-developer.com/empleados/lista

## Busqueda de Empleado por Email 

http://php-developer.com/empleados/busqueda?email=

## Detalle de Empleado por ID 

http://php-developer.com/empleados/id?id=

## XML de Empleados por rango de salario 

http://php-developer.com/empleados/xml?minimo=1500&maximo=2500
